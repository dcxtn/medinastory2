module.exports = {
  publicPath: './',
  pwa: {
    themeColor: '#d7c8c4',
    msTileColor: '#d7c8c4',
    manifestOptions: {
      name: 'Medinas Stories',
      short_name: 'Medinas',
    }
  }
}

