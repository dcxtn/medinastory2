import Vue from 'vue'
import Vuex from 'vuex'

import content from './content';

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    content,
    city: null,
    visitedCities: [],
    language: 'en',
    page: 'home',
    playing: false,
  },
  mutations: {
  },
  actions: {
  },
  modules: {
  }
})
