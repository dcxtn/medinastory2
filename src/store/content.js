export default {
    CA: {
        latitude: 45,
        longitude: -65,
        cx: 957.0591078047931,
        cy: 70.78510971311637,
        img: `CASBAH__OCEAN_p.jpg`,

        pages: [
            {
                img: '1024px-CASBAH__OCEAN.jpg',
                credit: 'Reda Ait Saada - Own work, CC BY-SA 4.0',
                fr: {
                    title: `Casbah d’Alger`,
                    description: `Inscrite au patrimoine mondial de l'UNESCO depuis 1992, la Casbah d’Alger est un exemple d'architecture islamique et d'urbanisme des médinas arabo-berbères, mais aussi un lieu de confluence de plusieurs cultures, religions et ethnies.`,
                    wiki: `https://fr.wikipedia.org/wiki/Casbah_d%27Alger`
                },
                en: {
                    title: `Casbah of Algiers`,
                    description: `Listed as a UNESCO World Heritage Site since 1992, the Kasbah of Algiers is an example of Islamic architecture and town planning in the Arab- Berber medinas, but also a place of confluence of several cultures, religions and ethnicities.`,
                    wiki: `https://en.wikipedia.org/wiki/Casbah_of_Algiers`
                },
                ar: {
                    title: `قصبة الجزائر`,
                    description: `تم إدراج قصبة الجزائر كموقع للتراث العالمي لليونسكو منذ عام 1992 مارة الاسالمية وتخطيط المدن في المدن العربية البربرية ، ولكنها أيضا مكان التقاء العديد من الثقافات .والاديان والاعراق .`,
                    wiki: `https://ar.wikipedia.org/wiki/قصبة_الجزائر`
                },
            },
            {
                img: 'La_grande_synagogue_dAlger_Djamâa_Li_houd-scaled.jpg',
                credit: 'Dzaynou CC-BY-SA 4.0',
                fr: {
                    title: `Djamaa Ben Fares`,
                    description: ` Djamaa Ben Fares était initialement une synagogue inaugurée en 1865, et restée pendant près de cent années l'un des lieux de culte de la communauté juive algéroise. En 1962, à l'indépendance du pays et suite à l'exode de la communauté juive, la synagogue est transformée en mosquée. On renomme officiellement le lieu Djamâa Farès, mosquée Farès mais pour beaucoup d'habitants elle reste nommée la Djamâa Li houd signifiant « la mosquée des juifs ».`,
                    wiki: `https://fr.wikipedia.org/wiki/Grande_synagogue_d%27Alger`
                },
                en: {
                    title: `Djamaa Ben Fares`,
                    description: `Abu Farès Mosque, originally built as the Great Synagogue of Algiers, is a mosque and former synagogue in Algiers. It is also known as the Djamâa Lihoud. In 1962, the synagogue was converted into the Abu Farès Mosque `,
                    wiki: `https://en.wikipedia.org/wiki/Djamaa_Ben_farès`
                },
                ar: {
                    title: `مسجد ابن فارس`,
                    description: `مسجد ابن فارس هو مسجد وكنيس سابق في الجزائر العاصمة. وكان يُطلَق عليه سابقاً كنيس الجزائر الكبير. كما أنه يُعرف باسم جامع اليهود بُني كنيس الجزائر الكبير في عام 1865 من الطوب على طراز المساجد المحيطة في سنة 1962تم  تحويل الكنيس إلى مسجد وعادت تسمية المكان رسميًا إلى مسجد ابن فارس، لكن بالنسبة للعديد من السكان لا يزال يُطلقون عليه جامع ا<i>ل</i>يهود `,
                    wiki: ` https://ar.wikipedia.org/wiki/مسجد_ابن_فارس `
                },
            },
            {
                img: 'Mosquée_Ketchaoua_Kasbah_p.jpg',
                credit: 'Dzaynou, CC-BY-SA 4.0',
                fr: {
                    title: `Mosquée Ketchaoua`,
                    description: `La mosquée Ketchaoua est une mosquée historique faisant partie du patrimoine classé de la basse casbah d'Alger. Construite en 1436, elle aurait été massivement remaniée au xviiie siècle sous le gouvernement du dey Hassan.   <br> Elle l'est à nouveau au xixe siècle, après sa réquisition en 1832, pour être affectée au culte catholique durant la période coloniale sous le nom de cathédrale Saint-Philippe . En 1962, elle redevient une mosquée.   `,
                    wiki: `https://fr.wikipedia.org/wiki/Mosquée_Ketchaoua`
                },
                en: {
                    title: `Ketchaoua Mosque`,
                    description: ` The Ketchaoua Mosque was built during the Ottoman rule in the 17th century and is located at the foot of the Casbah. The mosque is noted for its unique fusion of Moorish and Byzantine architecture. The mosque was originally built in 1612. Later, in 1845, it was converted during French rule, to the Cathedral of St Philippe, which remained so until 1962. `,
                    wiki: `https://en.wikipedia.org/wiki/Ketchaoua_Mosque`
                },
                ar: {
                    title: `جامع كتشاوة`,
                    description: `جامع كتشاوة من أشهر المساجد التاريخية بالعاصمة الجزائرية. بناه حسن باشا في العهد العثماني عام 1612م وجرى توسيعه عام 1794م، مما جعله أحد أكبر المساجد في الجزائر، ثم حوله  الدوق دو روفيغو سنة 1832 إلى كتدرائية القديس فيليب و استمر ذلك الي سنة 1963 تاريخ تحويله إلى جامع من جديد.`,
                    wiki: `https://ar.wikipedia.org/wiki/جامع_كتشاوة`
                },
            },
            {
                img: 'La_merdersa.jpg',
                credit: 'Yves Jalabert CC-BY 2.0',
                fr: {
                    title: `Medersa Thaâlibiyya`,
                    description: `La Medersa Thaâlibiyya est une medersa qui se trouve à la Kasbah d’Alger. Elle est bâtie en 1904 sous l'administration du gouverneur Charles Jonnart, qui fait la promotion du style néomauresque, appelé parfois « style Jonnart ». Ce style est aussi celui de nombreux bâtiments de l'époque, comme la Grande Poste d'Alger et la gare d'Oran. La medersa est construite pour rendre hommage au célèbre théologien maghrébin du xive siècle, Sidi Abderrahmane, considéré comme le saint patron de la ville d'Alger`,
                    wiki: `https://fr.wikipedia.org/wiki/Medersa_Thaâlibiyya`
                },
                en: {
                    title: `Medersa Thaalibiyya`,
                    description: `The Medersa Thaâlibiyya is a medersa located in the Kasbah of Algiers. It was built in 1904 under the administration of Governor C<i>h</i>arles Jonnart, who was promoting the neo-Moorish style, sometimes called “Jonnart style”. The medersa is named after the famous Maghrebian theologian of the fourteenth century, Sidi Abderrahmane, considered the saint patron of the city of Algiers and is located near his zaouia. `,
                    wiki: `https://en.wikipedia.org/wiki/Medersa_Thaalibiyya`
                },
                ar: {
                    title: `المدرسة الثعلبية`,
                    description: `المدرسة الثعلبية هي مدرسة تقع في قصبة الجزائر العاصمة. تم بناؤها في عام 1904 تحت إدارة الحاكم تشارلز جونارت ، الذي كان يروج للطراز المورسيكي الجديد ، الذي يطلق عليه أحيانًا 'أسلوب جونارت'.
                  سميت المدرسة على اسم  الفقيه المالكي والصوفي سيدي عبد الرحمن ، الذي يعتبر رمز مدينة الجزائر  التي أضحت تعرف بمدينة سيدي عبد الرحمان.
                  `,
                    wiki: ` https://ar.wikipedia.org/wiki/المدرسة_الثعلبية`
                },
            },
            {
                img: 'Arab-andalusian_music_of_Algiers_19th_c_p.png',
                credit: 'Alexandre Christianowitsch Domaine Public',
                fr: {
                    title: `Sanâa d'Alger `,
                    description: `La sanâa ou çanâa ou bien sanâa d'Alger ou simplement l'Andalou, désigne le répertoire de musique savante arabo-andalouse algérienne (classique) de l'école d'Alger et dont la tradition rattache à la ville de Cordoue en Espagne musulmane. Sanâa signifie « maîtrise musicale ». C'est le nom spécifique attribué en Algérie, à la Nouba de l'école d'Alger.`,
                    wiki: `https://fr.wikipedia.org/wiki/Sanâa_(musique)`
                },
                en: {
                    title: `Sanaa of Alger`,
                    description: `The sanâa or çanâa or sanâa of Algiers or simply Andalusian, designates the Arab-Andalusian Algerian music (classical) related to the school of Algiers and whose origin relates to the city of Cordoba in Muslim Spain. Sanâa means 'musical mastery'. This is the specific name given in Algeria to the Nouba of the school of Algiers. `,
                    wiki: `https://en.wikipedia.org/wiki/Sanaa_(music)`
                },
                ar: {
                    title: `الصنعة (موسيقى)`,
                    description: `الصنعة هو نوع من أنواع الموسيقى الجزائرية، يعود تاريخه إلى عودة الأندلسيين وحملهم لموسيقاهم الشعبية التي تعود للقرن العاشر الميلادي، نتيجة سقوط غرناطة، إلى بلاد المغرب العربي، وهناك تداخلت الموسيقى الشعبية الجزائرية مع الموسيقى الأندلسية، وكونت هذا الفن، والنوع من الموسيقى.`,
                    wiki: `https://ar.wikipedia.org/wiki/الصنعة_(موسيقى)`
                },
            },
            {
                img: 'Edmond_Nathan_Yafil.jpg',
                credit: 'Domaine Publique',
                fr: {
                    title: `Edmond Nathan Yafil `,
                    description: `Edmond N<i>a</i>than Yafil (1874; octobre 1928) est un compositeur algérien de confession juive. Né à Alger en 1874, Edmond Yafil a commencé sa carrière de musicien en fréquentant les cafés maures de la Casbah d'Alger où se perpétuait la tradition de la musique Sanâa. `,
                    wiki: `https://fr.wikipedia.org/wiki/Edmond_Nathan_Yafil`
                },
                en: {
                    title: `Edmond Yafil`,
                    description: `Edmond Nathan Yafil (1874 – October 1928) was an Algerian composer. Native Jew born in Algiers in 1874, Yafil began, as all the musicians of his time, by attending the Moorish cafés of the old Casbah Algiers places where perpetuated the tradition of Çan'a music, also referred to as Andalusian classical music. `,
                    wiki: ` https://en.wikipedia.org/wiki/Edmond_Yafil `
                },
                ar: {
                    title: `ادموند يافيل`,
                    description: `ادمون ناثان يافيل (1874-أكتوبر1928) هو ملحن جزائري يهودي الديانة ، ولد في الجزائر العاصمة عام 1874 . بدا يافيل مثله مثل جميع الموسيقيين في عصره بارتياد المقاهي في القصبة القديمة اين كانت موسقى الصنعة منتشرة بكثرة.  `,
                    wiki: `https://ar.wikipedia.org/wiki/إيدموند_ناثان_يافيل`
                },
            },
        ]

    },
    ES: {
        latitude: 39,
        longitude: -99,
        cx: 434.95114350072396,
        cy: 279.19415304827066,
        img: `Essaouira_Morocco_p.jpg`,
        pages: [
            {
                img: 'Essaouira_Morocco-scaled.jpg',
                credit: 'RyansWorld - Own work, CC BY-SA 3.0',
                fr: {
                    title: `Medina d’Essaouira`,
                    description: `Inscrite au patrimoine mondial de l'UNESCO depuis 2001, la Medina d’Essaouira offre l'exemple d'un centre multiculturel et multiconfessionnel. Dès sa conception, elle a connu la coexistence de diverses ethnies et religions.`,
                    wiki: `https://fr.wikipedia.org/wiki/Médina_d%27Essaouira`
                },
                en: {
                    title: `Medina of Essaouira`,
                    description: `Listed as a UNESCO World Heritage Site since 2001, the Medina of Essaouira offers an example of a multicultural and multiconfessional center. Since its conception, it has known the coexistence of various ethnic groups and religions.`,
                    wiki: `https://en.wikipedia.org/wiki/Medina_of_Essaouira`
                },
                ar: {
                    title: `مدينة الصويرة`,
                    description: `تم إدراج مدينة الصويرة كموقع للتراث العالمي لل1 ، وهي تقدم مثالًا على مركز متعدد الثقافات والاديان. إذ منذ ولدتها ، عرفت التعايش بين الجماعات العرقية والديانات المختلفة .`,
                    wiki: `https://ar.wikipedia.org/wiki/المدينة_العتيقة_للصويرة`
                },
            },
            {
                img: 'Bayt_Dakira_Simon_Attias_synagogue_Essaouira-scaled.jpg',
                credit: 'TomiValny CC-BY-SA 4.0',
                fr: {
                    title: `Bayt Dakira`,
                    description: `Litt: Maison de la Mémoire, Bayt Dakira est un musée et centre de recherche inauguré en janvier 2020, autour de la préservation de <i>l</i>a mémoire des juifs d’Essaouira. Avant sa réhabilitation, le bâtiment abritait la synagogue Simone Attias`,
                    wiki: `https://fr.wikipedia.org/wiki/Bayt_Dakira`
                },
                en: {
                    title: `Bayt Dakira`,
                    description: ` Litt: House of Memory, Bayt Dakira is a museum and research center inaugurated in January 2020, around the preservation of the memory of the Jews of Essaouira. Before its rehabilitation, the building housed the Simone Attias synagogue.`,
                    wiki: `https://en.wikipedia.org/wiki/Bayt_Dakira`
                },
                ar: {
                    title: `بيت الذاكرة `,
                    description: ` بيت الذاكرة هو متحف ومركز أبحاث حول الحفاظ على ذاكرة يهود الصويرة. و قد تم تدشينه في يناير 2020. قبل أن يتم ترميمها و تحويلها إلى متحف، كانت البناية تحتضن الكنيس 'صلاة عطية'. `,
                    wiki: `https://ar.wikipedia.org/wiki/بيت_الذاكرة`
                },
            },
            {
                img: 'SYNAGOGUE_SLAT_EL_KAHEL_-_MEDINA_ESSAOUIRA-scaled.jpg',
                credit: 'Elfathiamine CC-BY-SA 4.0',
                fr: {
                    title: `Synagogue Slat Lkahal `,
                    description: `Cette synagogue fut édifiée à partir de 1850, grâce aux fonds récoltés lors des cérémonies funéraires par des membres de la communauté qui se mêlaient à la foule lors des enterrements et demandaient l'aumône. 'Slat Lkahal', qui signifie 'Synagogue de la Communauté', est la seule synagogue du Maroc à se nommer ainsi. `,
                    wiki: `https://fr.wikipedia.org/wiki/Synagogue_Slat_Lkahal`
                },
                en: {
                    title: `Slat Lkahal Synagogue`,
                    description: `This synagogue was built around 1850, with the funds raised during funeral ceremonies by members of the community who mingled with the crowd at funerals and begged alms. 'Slat Lkahal', which means 'Synagogue of the Community', is the only synagogue in Morocco to have this name.`,
                    wiki: `https://en.wikipedia.org/wiki/Slat_Lkahal_Synagogue`
                },
                ar: {
                    title: `كنيس صلاة القهال`,
                    description: `تم بناء هذا الكنيس عام 1850 ، بفضل الأموال التي كان يتم جمعها خلال المراسم الجنائزية من قبل أفراد المجتمع اليهودي الذين كانو يختل<i>ط</i>ون مع الحشود في الجنازات لطلب الصدقات و الهبات. و تعني “صلاة القهال” كنيس المجتمع ، و هذا هو الكنيس الوحيد في المغرب الذي حمل هذا الاسم. `,
                    wiki: `https://ar.wikipedia.org/wiki/كنيس_صلاة_القهال`
                },
            },
            {
                img: 'Synagogue_Rabbi_Haim_Pinto.jpg',
                credit: 'Nassima Chahboun, CC-BY-SA 4.0',
                fr: {
                    title: `Hiloula de Rabbi Haim Pinto`,
                    description: `Chaque année, des centaines de pèlerins se réunissent à Essaouira pour rendre hommage au rabbin Haïm Pinto. Lors cette Hiloula, des juifs originaires d’Essaouira et d’ailleurs viennent se recueillir dans la synagogue familiale du Rabbin. Un autre lieu de recueillement est le mausolée de Rabbi Haïm Pinto dans l`,
                    wiki: `https://fr.wikipedia.org/wiki/Hiloula_de_Rabbi_Haïm_Pinto `
                },
                en: {
                    title: `Hiloula of Rabbi Haim Pinto`,
                    description: `Every year, hundreds of pilgrims gather in Essaouira to commemorate Rabbi Haim Pinto.   During this Hiloula, Jews from Essaouira and elsewhere come to meditate in the Rabbi's family synagogue. Another place of contemplation is the mausoleum of Rabbi Haïm Pinto in the marine cemetery.`,
                    wiki: `https://en.wikipedia.org/wiki/Hiloula_of_Rabbi_Haim_Pinto`
                },
                ar: {
                    title: `هيلولة ربي حاييم بينتو`,
                    description: `يتجمع مئات الحجاج سنويا في الصويرة لتكريم الحاخام حاييم بينتو. خلال هذه الهيلولة ، يأتي اليهود من الصويرة وأماكن أخرى للتعبد داخل كنيس عائلة الحاخام. كما يتخذون مكانا آخر للتعبد، هو ضريح الحاخام حاييم بينتو في المقبرة البحرية `,
                    wiki: `https://ar.wikipedia.org/wiki/هيلولة_ربي_حاييم_بينتو`
                },
            },
            {
                img: '1280px-Old_Jewish_cemetery_of_Essaouira-960x720.jpg',
                credit: 'TomiValny - Own work, CC BY-SA 4.0',
                fr: {
                    title: `Cimetières juifs d’Essaouira`,
                    description: ` L’histoire si riche de la communauté juive d’Essaouira se retrouve sur les tombes de ces cimetières. Plusieurs personnalités importantes y reposent, y compris Rabbi Haim Pinto. `,
                    wiki: `https://fr.wikipedia.org/wiki/Cimetières_Juifs_d'Essaouira`
                },
                en: {
                    title: `Jewish Cemeteries of Essaouira`,
                    description: `The rich history of the Jewish community of Essaouira is in the graves of these cemeteries. Several important personalities rest there, including Rabbi Haim Pinto. `,
                    wiki: `https://en.wikipedia.org/wiki/Jewish_cemeteries_of_Essaouira`
                },
                ar: {
                    title: `المقابر اليهودية بالصويرة`,
                    description: `تحكي هاته المقابر التاريخ الغني للمجتمع اليهودي بمدينة الصويرة. فالعديد من الشخصيات الهامة  في تاريخ المدينة دفنت هناك ، بما فيها الحاخام حاييم بينتو `,
                    wiki: `https://ar.wikipedia.org/wiki/المقابر_اليهودية_بالصويرة`
                },
            },
            {
                img: 'Le_festival_gnaoui_favorise_louverture_et_la_tolérance_7851916574-1536x1024.jpg',
                credit: 'Magharebia CC-BY 2.0',
                fr:
                {
                    title: `Festival Gnaoua`,
                    description: ` Un festival musical créé en 1997 afin de promouvoir la culture des Gnaouas (artistes d'origine d'Afrique subsaharienne). Il se tient au début de chaque été à Essaouira. Au fil des éditions, le festival a permis des milliers de rencontres entre musiciens provenant de quatre continents`,
                    wiki: `https://fr.wikipedia.org/wiki/Festival_Gnaoua_et_Musiques_du_monde`
                },
                en: {
                    title: `Gnawa Festival`,
                    description: `A musical festival created in 1997 to promote the culture of Gnawas (artists of sub-Saharan Africa). It is held at the beginning of each summer in Essaouira.                Over the years, the festival has allowed thousands of meetings between musicians from four contin<i>e</i>nts.             `,
                    wiki: `https://en.wikipedia.org/wiki/Gnaoua_World_Music_Festival`
                },
                ar: {
                    title: `مهرجان كناوة`,
                    description: ` أطلق هذا المهرجان الموسيقي سنة 1997 للترويج لثقافة كناوة ( و هم فنانوا إفريقيا جنوب الصحراء). و يقام في بداية كل صيف في الصويرة. على مر السنين ، سمح المهرجان بآلاف اللقاءات بين الموسيقيين من العالم بأسره `,
                    wiki: `https://ar.wikipedia.org/wiki/مهرجان_كناوة_وموسيقى_العالم`
                },
            },
        ]

    },

    FE: {
        latitude: 42,
        longitude: -88,
        cx: 614.6900163411959,
        cy: 208.85550678505322,
        description: `تم إدراج مدينة فاس كموقع للتراث العالمي لليونسكو منذ عام 1981،و هي لا تمثل فقط تراثا معمارًيا وعمرانًيا ، ولكنها تبرز أيضا نمط عيش وثقافة ذات تنوع فريد .`,
        img: `fes_p.jpg`,
        pages: [
            {
                img: '2018-12-27_12-38-06_IMG_7922_33103942288-1536x1015.jpg',
                credit: 'Max Dawncat from Russia, CC BY 2.0',
                fr: {
                    title: `Medina de Fès`,
                    description: `Inscrite au patrimoine mondial de l'UNESCO depuis 1981, la Médina de Fès ne représente pas seulement un patrimoine architectural et urbain, mais illustre également un mode de vie et une culture d’une diversité exceptionnelle.`,
                    wiki: `https://fr.wikipedia.org/wiki/Médina_de_Fès`
                },
                en: {
                    title: `Medina of FeZ`,
                    description: `Listed as a UNESCO World Heritage Site since 1981, the Medina of Fez not only represents an architectural and urban heritage, but also illustrates a way of life and a culture of exceptional diversity.`,
                    wiki: `https://en.wikipedia.org/wiki/Fes_el_Bali`
                },
                ar: {
                    title: `مدينة فاس `,
                    description: `تم إدراج مديث العالمي لليونسكو منذ عام 1981،و هي لا تمثل فقط تراثا معمارًيا وعمرانًيا ، ولكنها تبرز أيضا نمط عيش وثقافة ذات تنوع فريد .`,
                    wiki: `https://ar.wikipedia.org/wiki/فاس_البالي`
                }
            },
            {
                img: 'Andalous_mosque_portal.jpg',
                credit: 'Robert Prazeres - Own work, CC BY-SA 4.0',
                fr: {
                    title: `Mosquée des Andalous`,
                    description: `Cet ancien lieu de culte musulman et lieu historique est situé dans le quartier auquel elle a donné son nom. Elle est située sur la rive droite de l'oued de Fès, et doit son nom aux familles andalouses chassées de Cordoue par les Omeyyades et qui ont fondé le quart`,
                    wiki: `https://fr.wikipedia.org/wiki/Mosquée_des_Andalous`
                },
                en: {
                    title: `Andalusian Mosque`,
                    description: `This ancient Muslim place of worship and historic site  islocated in the district to which it gave its name. It is located on the right bank of the wadi of Fez, and owes its name to the Andalusian families chased from Cordoba by the Umayyads and who founded the Andalusian district in 818.`,
                    wiki: `https://en.wikipedia.org/wiki/Andalusian_Mosque`
                },
                ar: {
                    title: `جامع الأندلس`,
                    description: `يقع مكان المبنى التاريخي في المنطقة التي أطلق عليها اسمه. يتواجد المسجد على الضفة اليمنى لوادي فاس ، و يرجع اسمه للعائلات الأندلسية التي قام الأمويون بطردها من قرطبة والتي أسست الحي الأندلسي عام 818.`,
                    wiki: `https://ar.wikipedia.org/wiki/جامع_الأندلس`
                },
            },
            {
                img: 'Fes_El_Bali_Fes_Morocco_-_panoramio.jpg',
                credit: 'Ben Bender CC-BY-SA 3.0',
                fr: {
                    title: `Ziarra Tijania`,
                    description: `Fès est le berceau de la tarîqa Tijaniya et la ville de la Zaouia Tijania Al Koubra. Chaque année, ses adeptes aux quatre coins du monde affluent vers La Zaouia pour 'la ziarra' (la visite) pour fêter de nombreuses occasions, notamment Laylat Al Qadr (27 du Ramadan) et Al Mawlid Nabawi (Jour de la naissance du prophète). `,
                    wiki: `https://fr.wikipedia.org/wiki/Ziarra_Tijaniya`
                },
                en: {
                    title: `Ziarra Tijania`,
                    description: `Fez is the cradle of Tarîqa Tijaniya and the city of Zaouia Tijania Al Koubra. Every year, its followers from all over the world flock to La Zaouia for 'la ziarra' (the visit) to celebrate many occasions, such as Laylat Al Qadr (27 of Ramadan) and Al Mawlid Nabawi (Day of the Prophet's birth) .              `,
                    wiki: `https://en.wikipedia.org/wiki/Ziarra_Tijaniya`
                },
                ar: {
                    title: `الزيارة التيجانية`,
                    description: `تعد فاس مهد الطريقة التيجانية ومدينة الزاوية التيجانية الكبرى. كل عام ، يتوافد أتباعها من جميع أنحاء العالم إلى الزاوية من أجل 'الزيارة' و الاحتفال بالعديد من المناسبات ، بما في ذلك ليلة القدر 27 من ر<i>ر</i>مضان) والمولد النبوي (يوم المولد النبوي) .                          `,
                    wiki: `https://ar.wikipedia.org/wiki/الزيارة_التيجانية`
                },
            },
            {
                img: '800px-Ibn_Danan_synagogue_Fes.jpg',
                credit: 'TomiValny - Own work, CC BY-SA 4.0',
                fr: {
                    title: `Synagogue Ibn Danan`,
                    description: `La synagogue Ibn Danan est une synagogue datant du 17ème siècle. Elle a été construite par Mimoun Ben Sidan, un riche marchand de la ville d'Ait Ishaq. La synagogue est située dans le quartier du Mellah à Fès el-Jdid. `,
                    wiki: `https://fr.wikipedia.org/wiki/Synagogue_Ibn_Danan`
                },
                en: {
                    title: `Ibn Danan Synagogue`,
                    description: `Ibn Danan Synagogue is a 17th century synagogue. It was built by Mimoun Ben Sidan, a wealthy merchant from the town of Ait Ishaq. The synagogue is located in the Mellah district in Fez el-Jdid.`,
                    wiki: `https://en.wikipedia.org/wiki/Ibn_Danan_Synagogue`
                },
                ar: {
                    title: `كنيس ابن دنان`,
                    description: `يعود كنيس ابن دنان إلى القرن السابع عشر. و قد تم بناءه من طرف ميمون بن سيدان ، وهو تاجر ثري من بلدة آيت إسحاق. يقع الكنيس في حي الملاح بفاس الجديد.                `,
                    wiki: ` https://ar.wikipedia.org/wiki/كنيس_ابن_دنان`
                },
            },
            {
                img: 'MoroccoFes_synagogue2.jpg',
                credit: 'Csörföly Dániel CC-BY-SA 3.0',
                fr: {
                    title: `Synagogue Al Fassiyine`,
                    description: `La synagogue Al Fassiyine, ou Salat Al Fassiyine, est une synagogue datant du 16ème siècle. Elle se situe au Mellah, au sein de la Casbah Mérinide, et est considérée comme l’un des centres religieux éminents chez les juifs Marocains. `,
                    wiki: `https://fr.wikipedia.org/wiki/Synagogue_Al_Fassiyine`
                },
                en: {
                    title: `Al Fassiyine Synagogue`,
                    description: `Al Fassiyine Synagogue, or Salat Al Fassiyine, is a synagogue dating back to the 16th century. It is located in Mellah, within the Merinide Casbah, and is considered one of the eminent religious centers a<i>m</i>ong Moroccan Jews.`,
                    wiki: `https://en.wikipedia.org/wiki/Al_Fassiyine_Synagogue`
                },
                ar: {
                    title: `كنيس صلاة الفاسيين`,
                    description: `يعود كنيس صلاة الفاسيين إلى القرن السادس عشر، و يتواجد بالملاح، داخل القصبة المرينية.  و يعتبر هذا الكنيس أحد أهم المراكز الدينية عند اليهود المغاربة.`,
                    wiki: `https://ar.wikipedia.org/wiki/كنيس_صلاة_الفاسيين`
                },
            },
            {
                img: 'Semakar_white.svg',
                credit: 'Michel Bakni  CC-BY-SA 4.0',
                fr: {
                    title: `Festival de Fès de la Culture Soufie`,
                    description: `Ce festival a pour but de promouvoir les arts relatifs au soufisme. Il comprend des soirées de ‘Dhikr’ organisées par des confréries issues de différents pays et cultures, ainsi que des manif<i>e</i>stations artistiques et musicales. `,
                    wiki: ` https://fr.wikipedia.org/wiki/Festival_de_Fès_de_la_Culture_Soufie`
                },
                en: {
                    title: `Fez Sufi Culture Festival`,
                    description: `This festival aims to promote the arts related to Sufism. It includes ‘Dhikr’ ceremonies organized by brotherhoods from different countries and cultures, as well as artistic and musical events.`, wiki: `https://en.wikipedia.org/wiki/Fez_Sufi_Culture_Festival`
                },
                ar: {
                    title: `مهرجان فاس للثقافة الصوفية `,
                    description: `يهدف هذا المهرجان إلى الترويج للفنون المتعلقة بالصوفية. ويشمل أمسيات الذكر التي تنظمها طرائق من مختلف البلدان والثقافات ، فضلاً عن الفعاليات الفنية والموسيقية.`,
                    wiki: `https://ar.wikipedia.org/wiki/مهرجان_فاس_للثقافة_الصوفية`
                }
            },
        ]


    },

    KA: {
        latitude: 38,
        longitude: -50,
        cx: 1241.0685978860126,
        cy: 114.78752194665418,
        img: `Vue_aérienne_de_la_Grande_Mosquée_de_Kairouan_en_Tunisie_p.jpg`,
        pages: [
            {
                img: 'Vue_aérienne_de_la_Grande_Mosquée_de_Kairouan_en_Tunisie-768x375.jpg',
                credit: 'Momin Bannani from London, UK, CC BY-SA 2.0',
                fr: {
                    title: `Medina de Kairouan`,
                    description: `Inscrite au patrimoine mondial de l'UNESCO depuis 1988, La Medina de Kairouan, ville  était aussi un important foyer chrétien où la communauté chrétienne était florissante jusqu'à l'arrivée des Fatimides.`,
                    wiki: `https://fr.wikipedia.org/wiki/Médina_de_Kairouan`
                },
                en: {
                    title: `Medina of Kairouan`,
                    description: `Listed as a UNESCO World Heritage Site since 1988, The Medina of Kairouan, the holy city of Islam, was also an important Christian home where the Christian community flourished until the arrival of the Fatimids.`,
                    wiki: `https://en.wikipedia.org/wiki/Medina_of_Kairouan`
                },
                ar: {
                    title: `مدينة القيروان`,
                    description: `تم إدراج مدينة القيروان ، المدينة الاسالمية اث العالمي لليونسكو منذ عام 1988 ، وقد كانت أيضا موطنا مسيحًيا مهًما حيث ازدهر المجتمع المسيحي بها حتى وصول الفاطميين .`,
                    wiki: `https://ar.wikipedia.org/wiki/القيروان`
                },
            },
            {
                img: "كنيسة_القيروان_صورة_1-1068x1536.jpg",
                credit: 'Dorra la kairouanaise  CC-BY-SA 4.0',
                fr: {
                    title: `La Synagogue de Kairouan`,
                    description: `La Synagogue de Kairouan a été inauguré en 1920 par la communauté juive de la ville qui a choisit en 1910 d’acquérir une parcelle enclavée en plein cœur de la médina pour édifier la synagogue et affirmer, ainsi, « sa volonté de renouer avec un passé où la ville sainte de l’islam était aussi un très important foyer culturel juif ». `,
                    wiki: `https://fr.wikipedia.org/wiki/Synagogue_de_Kairouan`
                },
                en: {
                    title: `Kairouan Synagogue`,
                    description: `The Kairouan Synagogue was inaugurated in 1920 by the local Jewish community who decided in 1910 to acquire a parcel in the heart of the medina to build the synagogue and to affirm its desire to reconnect with a past. where this holy city of Islam was also a very important Jewish cultural center.`, 
                    wiki: `https://en.wikipedia.org/wiki/Synagogue_of_Kairouan`
                },
                ar: {
                    title: `كنيس القيروان`,
                    description: `تم افتتاح كنيس القيروان في عام 1920 من قبل الجالية اليهودية المحلية ، والتي قررت سنة 1910 الحصول على قطعة أرض في قلب المدينة لبناء الكنيس وبالتالي تأكيد رغبتها في إعادة ماضي غير بعيد عندما كانت القيروان مركزًا ثقافيًا يهوديًا مهمًا للغاية.`,
                    wiki: `https://ar.wikipedia.org/wiki/كنيس_القيروان`
                },
            },
            {
                img: "أمراة_تزور_مقام_سيدي_عبيد_الغرياني.jpg",
                credit: 'Ayman Chermiti - Own work, CC BY-SA 4.0',
                fr:
                {
                    title: `Mausolée Sidi Abid el Ghariani`,
                    description: `Le mausolée Sidi Abid el Ghariani, ancienne zaouïa élevée durant la seconde moitié du xive siècle puis agrandie et embellie au xviie siècle. Le monument se distingue par sa décoration raffinée mêlant céramiques, plâtre finement ciselé et plafonds en bois peint et sculpté. Le mausolée porte le nom de Abou Samir Abid el Ghariani, qui fait du mausolée un lieu où il dispense son enseignement pendant vingt ans.`,
                    wiki: `https://fr.wikipedia.org/wiki/Mausolée_Sidi_Abid_el_Ghariani`
                },
                en: {
                    title: `Sidi Abid El Ghariani Mausoleum`,
                    description: `The Sidi Abid el Ghariani mausoleum is a former zaouïa erected during the second half of the fourteenth century then enlarged and embellished in the seventeenth century. The monument is distinguished by its refined decoration combining ceramic and painted and carved wooden ceilings. The mausoleum is named after Abu Samir Abid el Ghariani, who made the mausoleum a learning place where he teached religion for twenty years.`, 
                    wiki: `https://en.wikipedia.org/wiki/Sidi_Abid_El_Ghariani_Mausoleum`
                },
                ar: {
                    title: `ضريح سيدي عبيد الغرياني`,
                    description: `ضريح سيدي عبيد الغرياني هو زاوية شيدت خلال النصف الثاني من القرن الرابع عشر ثم تم توسيعها وزخرفتها في القرن السابع عشر. يتميز البناء بزخارفه الراقية التي تجمع بين الخزف والأسقف الخشبية المطلية والمنقوشة. سمي الضريح على أبو سمير عبيد الغرياني الذي جعل من المكان مدرسة و قام بتدريس الدين لمدة عشرين عامًا. `,
                    wiki: `https://ar.wikipedia.org/wiki/ضريح_سيدي_عبيد_الغرياني`
                },
            },
            {
                img: 'Tunisie_Enfants_juifs.jpg',
                credit: 'By Rais67 - Personal Collection, Public Domain',
                fr: {
                    title: `Yechiva de Kairouan  `,
                    description: `La yechiva, ou yeshivah est un centre d'étude de la Torah et du Tal<i>m</i>ud dans le judaïsm. La yechiva de Kairouan fondée par Ḥushi'el b. Elhanan est considérée comme étant la première importante yeshiva en afrique du nord. Elle était très lié à la yechiva de Babylonie et a connu son apogée durant le Xème siècle. `,
                    wiki: `https://fr.wikipedia.org/wiki/Yechiva_de_Kairouan`
                },
                en: {
                    title: `Kairouan Yeshiva`,
                    description: `The yeshiva, or yeshiva, is a center for the study of Torah and Talmud in Judaism. The yechiva of Kairouan founded by Ḥushi'el b. Elhanan is considered to be the first major yeshiva in North Africa. It was closely linked to the yeshiva of Babylonia and became very important during the 10th century.   `, 
                    wiki: `https://en.wikipedia.org/wiki/Kairouan_Yeshiva`
                },
                ar: {
                    title: `يشيفا القيروان`,
                    description: `، اليشيفا ، هي مدرسة دينية لدراسة التوراة والتلمود عند اليهود. تعتبر يشيفا القيروان التي أسسها هوشي الحنان أول مدرسة دينية كبرى في شمال إفريقيا . كانت يشيفا القيروان مرتبطة ارتباطًا وثيقًا بالمدرسة الدينية في بابل وأصبحت مهمة جدًا خلال القرن العاشرميلادي..`,
                    wiki: `https://ar.wikipedia.org/wiki/يشيفا_القيروان`
                },
            },
            {
                img: "صورة_مقام_سيدي_عمر_عبادة-1153x1536.jpg",
                credit: 'Ayman Chermiti - Own work, CC BY-SA 4.0',
                fr: {
                    title: `Mausolée Sidi Amor Abbada`,
                    description: `Le mausolée Sidi Amor Abbada, construit vers 1872, est un édifice abritant le tombeau d'un célèbre marabout du xixe siècle et dont le mausolée porte le nom.. Le bâtiment est remarquable à l'extérieur grâce à un ensemble de six coupoles côtelées. `,
                    wiki: `https://fr.wikipedia.org/wiki/Mausolée_Sidi_Amor_Abbada`
                },
                en: {
                    title: `Sidi Ammor Abbada Mausoleum`,
                    description: `The Sidi Amor Abbada mausoleum, is a mausoleum in the medina of Kairoun named after the s<i>u</i>fi marabout Sidi Ali Abada. The building is remarkable on the outside thanks to a set of six ribbed domes.`, 
                    wiki: `https://en.wikipedia.org/wiki/Sidi_Ammor_Abbada_Mausoleum`
                },
                ar: {
                    title: `زاوية سيدي عمر عبادة`,
                    description: `زاوية سيدي عمر عبادة أو زاوية الشيخ عمر عبادة هي زاوية دينية تقع في القيروان، شيدت سنة 1872 وتحتوي قبر أبو حفص عمر بن سالم عبادة العياري، وهو مرابط صوفي عاش في النصف الأول من القرن التاسع عشر. ويمتاز البناء بقبابه الخارجية الستة التي ترتكز على حوامل ذات طابع قيرواني خاص. `,
                    wiki: ` https://ar.wikipedia.org/wiki/زاوية_سيدي_عمر_عبادة`
                },
            },
            {
                img: 'Eglise_de_Kairouan.jpg',
                credit: 'Lévy et fils PD-Tunisia ',
                fr: {
                    title: `Christianisme à Kairouan`,
                    description: `Kairouan, la ville sainte de l’islam était aussi un important foyer chrétien où la communauté chrétienne était florissante jusqu'à l'arrivée des Fatimides. Les différentes communautés religieuses de la ville vivaient dans une promiscuité paisible. Outre leur nom de baptême issu du calendrier liturgique, les chrétiens kairouanias pouvaient avoir des noms arabes, tel ce Bakr al-Wahid, cavalier réputé, ou Ibn Wardah, riche marchand de la ville.  `,
                    wiki: `https://fr.wikipedia.org/wiki/Histoire_des_Chrétiens_à_Kairouan`
                },
                en: {
                    title: `Christian Kairouan`,
                    description: `Kairouan, the holy city of Islam was also an important Christian center where the Christian community flourished until the arrival of the Fatimids. The various religious communities of the city lived peacefuly. For instance, besides to their baptismal name from the liturgical calendar, local Christians used to have Arabic names, such as Bakr al-Wahid, a famous knight, or Ibn Wardah, a rich merchant in the city.`, 
                    wiki: `https://en.wikipedia.org/wiki/History_of_the_Christians_in_Kairouan`
                },
                ar: {
                    title: `ِالمسيحية في القيروان`,
                    description: `كانت القيروان ، المدينة الإسلامية المقدسة ، أيضًا مركزًا مسيحيًا مهمًا حيث ازدهر المجتمع المسيحي حتى وصول ال<i>ف</i>اطميين. عاشت المجتمعات الدينية المختلفة في المدينة بسلام. على سبيل المثال ، إلى جانب اسم المعمودية من التقويم الليتورجي ، اعتاد المسيحيون المحليون على استخدام أسماء عربية ، مثل الفارس الشهير بكر الوحيد أو ابن وردة ، وهو تاجر ثري في المدينة `,
                    wiki: `https://ar.wikipedia.org/wiki/تاريخ_المسيحين_في_القيروان`
                }
            },
        ]
    },

    MA: {
        latitude: 39,
        longitude: -93,
        cx: 536.0488564992761,
        cy: 279.19415304827066,
        img: `Medina_of_Marrakesh_5_p.jpg`,
        pages: [
            {
                img: 'Medina_of_Marrakesh_5-1024x768.jpg',
                credit: 'Dieglop - Own work, CC BY-SA 4.0',
                fr: {
                    title:`Medina de Marrakech`,
                    description: `Inscrite au patrimoine mondial de l'UNESCO depuis 1985, la Medina de Marrakech fut longtemps un centre culturel majeur de l'Occident musulman, régnant sur l'Afrindalousie.`,
                    wiki:`https://fr.wikipedia.org/wiki/Médina_de_Marrakech`,
                },
                en: {
                    title:`Mellah of Marrakesh`,
                    description: `Listed as a UNESCO World Heritage Site since 1985, the Medina of Marrakech has long been a major cultural center of the Muslim West, reigning over North Africa and Andalusia.`,
                    wiki:`https://en.wikipedia.org/wiki/Marrakesh`,
                },
                ar: {
                    title:`مدينة مراكش`,
                    description: `تم إدراج مدينة مراكش كموقع تراث عالمي لليونسكو منذ عام 1985 ، و قد كانت لفترة طويلة مركزا ثقافًيا رئيسًيا في الغرب الاسالمي ، يشمل إشعاعه  شمال إفريقيا والاندلس.`,
                    wiki:`https://ar.wikipedia.org/wiki/المدينة_القديمة_(مراكش)`,
                },
            },
            {
                img: 'A02_-_Marrakech_Mellah-1536x1024.jpg',
                credit: 'Misa.stefanovic.07   CC-BY-SA 4.0',
                fr: {
                    title:`Mellah de Marrakech`,
                    description:`Le Mellah de Marrakech
                    ,(מלאח מרקש :hébreu(
                   anciennement connu sous le
                   nom de Hay Essalam est le
                   quartier juif de la ville de
                   Marrakech, au Maroc. C'est
                   le deuxième plus ancien
                   quartier juif dans le pays.
                   En 2016, le roi Mohamed VI
                   a ordonné de restaurer les
                   noms des rues qui avaient à
                   voir avec l'héritage juif de la
                   ville, y compris la
                   restauration du nom du
                   quartier à "El Mellah".`,
                    wiki:`https://fr.wikipedia.org/wiki/Mellah_de_Marrakech`,
                },
                en: {
                    title:`Mellah of Marrakesh`,
                    description: `The Mellah of Marrakech (Hebrew: מלאח מרקש), formerly known as Hay Essalam is the Jewish quarter of the city of Marrakech, Morocco. It is the second oldest Jewish quarter in the country.
                    In 2016, King Mohamed VI ordered the restoration of the names of the streets that had to do with the city's Jewish heritage, including the restoration of the neighborhood name to "El Mellah".
                    `,
                    wiki:`https://en.wikipedia.org/wiki/Mellah_of_Marrakesh`,
                },
                ar: {
                    title:`ملاح مراكش`,
                    description: `يعد ملاح مراكش، المعروف قديما باسم حي السلام، ثاني أقدم حي يهودي في المغرب.
                    سنة 2016، أعطى الملك محمد السادس توجيهات لترميم أسماء الأ<i>ز</i>قة التي تحمل اسما يهوديا، و كذلك إعادة الاسم القديم للملاح (بدل حي السلام).`,
                    wiki:`https://ar.wikipedia.org/wiki/ملاح_مراكش`,
                },
            },
            {
                img: 'Mellah_in_Marrakech_-_panoramio-768x1024.jpg',
                credit: 'Angela Stefanoni   CC-BY 3.0',
                fr: {
                    title:`Synagogue Salat Al Azama`,
                    description: `Litt: "synagogue des
                    dissidents". Elle est située
                    dans le mellah et est
                    actuellement occupée par
                    une famille musulmane, qui
                    en prend soin.
                    Les juifs de Marrakech la
                    considèrent comme la plus
                    ancienne synagogue de la
                    ville.
                    Elle a avait été Initialement
                    bâtie en 1492, année
                    d'expulsion des Juifs
                    d'Espagne. Or, le bâtiment
                    actuel date du tournant du
                    xixe siècle et du xxe siècle. `,
                    wiki:`https://fr.wikipedia.org/wiki/Synagogue_Salat_Al_Azama_(Marrakech)`,
                },
                en: {
                    title:`Salat Al Azama Synagogue`,
                    description: `The Slat al-Azama Synagogue or Lazama Synagogue is one of the best-known synagogues in Marrakesh, Morocco. It is located in the historic Mellah (Jewish quarter) of the old city. The synagogue was associated with Sephardic Jews who were expelled from Spain in 1492. The synagogue's foundation is likewise traditionally attributed to 1492,though one scholar has indicated that the exact year of establishment has not been verified.`,
                    wiki:`https://en.wikipedia.org/wiki/Slat_al-Azama_Synagogue`,
                    
                },
                ar: {
                    title:`كنيس صلاة الأزمة`,
                    description: `يعني اسم هذا الكنيس "كنيس المنشقين". وهو تقع في الملاح وتشغله حاليا أسرة مسلمة للاعتناء به،
                    ويعتبره يهود مراكش أقدم كنيس يهودي 
                    في المدينة.
                    تم بناء الكنيس في الأصل عام 1492 ، وهو العام الذي طرد فيه اليهود من إسبانيا. لكن فإن المبنى الحالي يعود تاريخه إلى مطلع القرن التاسع عشر والقرن العشرين
                      `,
                    wiki:`https://ar.wikipedia.org/wiki/كنيس_صلاة_الأزمة`,
                },
            },
            {
                img: '1920px-Coucher_de_soleil_sur_la_place_Jamaa_El_Fna-1200x675.jpg',
                credit: 'MoroccanPixels - Own work, CC BY-SA 4.0',
                fr: {
                    title:`Jemaa el-Fnaa<`,
                    description: `La place Jemaa el-Fna est une célèbre place publique
                    au sud-ouest de la médina de Marrakech au Maroc.
                    Véritable Cour des Miracles, aux portes des souks de
                    Marrakech, proche de la mosquée Koutoubia et du
                    palais royal, cette place est une des principales
                    attractions traditionnelles et historiques au Maroc. Elle
                    est animée d'une importante vie populaire de la fin de
                    l'après midi jusqu'à l'appel à la prière à l'aube.
                    En 1985, la place Jemaa el-Fna est inscrite sur la liste
                    du patrimoine mondial de l'Unesco dans le cadre de
                    l'inscription de la médina de Marrakech. De plus, «
                    l'espace culturel de la place Jemaa el-Fna » est inscrit
                    patrimoine culturel immatériel en 2008 par l'Unesco. `,
                    wiki:`https://fr.wikipedia.org/wiki/Place_Jemaa_el-Fna`,
                },
                en: {
                    title:`Jemaa El-Fna square`,
                    description: `Jemaa el-Fnaa is a square and market place in
                    Marrakesh's medina quarter (old city). It remains the
                    main square of Marrakesh, used by locals and tourists.
                    The place is known for its active concentration of
                    traditional activities by storytellers, musicians and
                    performers.
                    Listed in 1985 as part of the Medina of Marrakesh as a
                    UNESCO World Heritage, it has been also listed as a
                    Masterpiece of the Oral and Intangible Heritage of
                    Humanity in 2008. `,
                    wiki:`https://en.wikipedia.org/wiki/Jemaa_el-Fnaa`,
                },
                ar: {
                    title:`الفنا جامع ساحة`,
                    description: `ساحة جامع الفنا هو فضاء شعبي للفرجة والترفيه للسكان المحليين والسياح في
                    مدينة مراكش بالمغرب، وبناء على ذلك تعتبر هذه الساحة القلب النابض لمدينة
                    مراكش حيث كانت وما زالت نقطة التقاء بين المدينة والقصبة المخزنية
                    والمالح، ومحجا للزوار من كل أنحاء العالم لالستمتاع بمشاهدة عروض مشوقة
                    لمروضي األفاعي ورواة األحاجي والقصص، والموسيقيين إلى غير ذلك من
                    مظاهر الفرجة الشعبية التي تختزل تراثا غنيا وفريدا كان من وراء إدراج هذه
                    الساحة سنة 2008 في قائمة التراث الالمادي اإلنساني التي أعلنتها منظمة
                    اليونيسكو . `,
                    wiki:`https://ar.wikipedia.org/wiki/ساحة_جامع_الفنا`,
                },
            },
            {
                img: 'MarrakechBelAbbes-1536x1025.jpg',
                credit: 'Bertramz  CC-BY-SA 3.0',
                fr: {
                    title:`Zaouia de Sidi Bel Abbès `,
                    description: `Cette zaouia fut édifiée pour
                    honorer la mémoire du saintpatron de la ville Abu Al
                    Abbès as-Sabti.
                    De nos jours, elle joue un rôle
                    de premier plan dans la vie
                    sociale de Marrakech. Des
                    centaines de personnes,
                    ayant en commun d'être à la
                    fois indigents et handicapés,
                    bénéficient de ses actions
                    caritatives. `,
                    wiki:`https://fr.wikipedia.org/wiki/Zaouia_de_Sidi_Bel_Abbès`,
                },
                en: {
                    title:`Zawiya of Sidi Bel Abbes`,
                    description: `This zaouia was built to honor the memory of the patron saint of the city Abu Al Abbes as-Sabti.
                    Nowadays, it plays a leading role in the social life of Marrakech. Hundreds of people, having in common to be both destitute and disabled, benefit fr<i>o</i>m his charitable actions.
                   `,
                    wiki:`https://en.wikipedia.org/wiki/Zawiya_of_Sidi_Bel_Abbes`,
                },
                ar: {
                    title:`زاوية سيدي بلعباس `,
                    description: `تم بناء هذه الزاوية تكريما لذكرى شفيع المدينة أبو العباس السبتي.
                    في الوقت الحاضر ، تلعب الزاوية دورًا رائدًا في الحياة الاجتماعية لمراكش. إذ يستفيد المئات من الناس ، من المعوزين ذوي الاحتياجات الخاصة ، من أعمالها الخيرية
                    .`,
                    wiki:`https://ar.wikipedia.org/wiki/زاوية_سيدي_بلعباس`,
                },
            },
          
        ]
       

    },

    ME: {
        latitude: 41.3,
        longitude: -89.2,
        cx: 596.4327370689499,
        cy: 225.6561095713673,
        img: `Meknes_p.jpg`,
        pages: [
            {
                img: 'Meknes-1024x714.jpg',
                credit: 'Bernard Gagnon - Own work, CC BY-SA 3.0',
                fr: {
                    title:`Medina de Meknès`,
                    description: `Inscrite au patrimoine mondial de l'UNESCO depuis 1996, la Medina de Meknès fut une impressionnante cité de style hispano- qui montre aujourd'hui l'alliance harmonieuse des styles islamique et européen dans le Maghreb du XVIIe siècle.`,
                    wiki:`https://fr.wikipedia.org/wiki/Médina_de_Meknès`,
                },
                en: {
                    title:`Medina of Meknes`,
                    description: `Listed as a UNESCO World Heritage Site since 1996, the Medina of Meknes was an impressive city of Hispano-style which today shows the harmonious alliance of Islamic and European styles in the Maghreb of the 17th century.`,
                    wiki:`https://en.wikipedia.org/wiki/Medina_of_Meknes`,
                },
                ar: {
                    title:`مدينة مكناس`,
                    description: `تم إدراج مدينة مكناس كموقع تراث عالمي لليونسكو منذ عام 1996 ،وقد كانت المدينة نموذجا مذهلا  للطراز الاسباني ، ولا زالت إلى يومنا هذا شاهدة على التمازج و التناغم بين األنماط الاسالمية والاوروبية في المغرب  العربي في القرن السابع عشر .`,
                    wiki:`https://ar.wikipedia.org/wiki/مدينة_مكناس_التاريخية`,
                },
            },
            {
                img: 'Aissawa_Fes_Morocco.event_traditional_folklore_DSC03881.jpg',
                credit: 'Hakim-agh - Own work, CC BY-SA 4.0',
                fr: {
                    title:`Confrérie de Aïssawa`,
                    description: `La confrérie des Aïssawa est
                    un ordre mystico-religieux
                    fondé à Meknès par
                    Muhammad ben Aïssâ (1465-
                    1526 / 882-933 H.),
                    surnommé le « Maître Parfait
                    » (Chaykh al-Kâmil) et
                    originaire de la ville de
                    Taroudant. Les termes
                    Aïssâwiyya (‘Isâwiyya) et
                    Aïssâwa (‘Isâwa), issus du
                    nom du fondateur, désignent
                    respectivement la confrérie
                    (tariqa, litt. « voie ») et ses
                    disciples (fuqarâ, sing. fakir,
                    litt. « pauvre »). `,
                    wiki:`https://fr.wikipedia.org/wiki/Aïssawa`,
                },
                en: {
                    title:`Isawiyya`,
                    description: `The Aïssawa brotherhood is a mystical-religious order founded in Meknes by Muhammad ben Aïssâ (1465-1526 / 882-933 H.), nicknamed the “Perfect Master” (Chaykh al-Kâmil) and originally from the city of Taroudant. The terms Aïssâwiyya (‘Isâwiyya) and Aïssâwa (‘ Isâwa), derived from the name of the founder, respectively designate the brotherhood (tariqa, lit. "Way") and its disciples (fuqarâ, sing. Fakir, lit. "Poor").`,
                    wiki:`https://en.wikipedia.org/wiki/Isawiyya`,
                },
                ar: {
                    title:`الطريقة العيساوية`,
                    description: `  الطريقة العيساوية هي جماعة دينية صوفية تم تأسيسها في مكناس على يد محمد بن عيسى (1465-1526 / 882-933 هـ) ، الملقب بـ "المعلم الكامل" (الشيخ الكامل) و الذي يرجع أصله  إلى مدينة تارودانت.
                    تم اشتقاق المصطلحين "عيساوية" و "عيساوة"  من اسم المؤسس (بن عيسى) ، و يشيران على التوالي إلى  الطريقة وإلى تلاميذها (الذين يسمون أيضا الفقراء).
                    `,
                    wiki:`https://ar.wikipedia.org/wiki/الطريقة_العيساوية`,
                },
            },
            {
                img: 'ضريح_الشيخ_الكامل_-_Sheikh_El_Kamel_Mausoleum_01-768x576.jpg',
                credit: ' ZAINEB HACHAMI   CC-BY-SA 4.0',
                fr: {
                    title:`Mausolée de Cheikh Al Kamel`,
                    description: `Le mausolée de Cheikh El
                    Kamel (El hadi Benaïssa) se
                    situe à Bab Siba à Meknès. Il
                    est le cœur de la rencontre
                    des Aïssaoua.
                    Ce mausolée fut édifié par le
                    Sultan Sidi Mohamed Ben
                    Abdellah en 1776.`,
                    wiki:`https://fr.wikipedia.org/wiki/Mausolée_de_Cheikh_Al_Kamel`,
                },
                en: {
                    title:`Mausoleum of Cheikh Al Kamel`,
                    description: `The mausoleum of Sheikh El Kamel (El hadi Benaïssa) is located at Bab Siba in Meknes. It is the hea<i>r</i>t of the meeting of Aïssaoua. This mausoleum was built by Sultan Sidi Mohamed Ben Abdellah in 1776.`,
                    wiki:`https://en.wikipedia.org/wiki/Mausoleum_of_Cheikh_Al_Kamel`,
                },
                ar: {
                    title:`ضريح الشيخ الكامل`,
                    description: `يقع ضريح الشيخ الكامل (الهادي بن عيسى) في باب السيبة بمكناس. و هو محج و ملتقى متتبعي الطريقة العيساوية.  
                    شيد هذا الضريح السلطان سيدي محمد بن عبد الله عام 1776.`,
                    wiki:`https://ar.wikipedia.org/wiki/ضريح_الشيخ_الكامل`,
                },
            },
            {
                img: 'École_Hébraïque_Talmud_Tourah_de_Meknès_-_المدرسة_العبرانية_تلموت_توراة_بمكناس_07-768x1024.jpg',
                credit: 'ZAINEB HACHAMI   CC-BY-SA 4.0',
                fr: {
                    title:`Ecole Hébraique Talmud Torah`,
                    description: `L’école Talmud Torah est une école rabbinique située au nouveau Mellah (quartier juif) de Meknès. `,
                    wiki:`https://fr.wikipedia.org/wiki/Ecole_Hébraique_Talmud_Torah_de_Meknès`,
                },
                en: {
                    title:`Hebrew Talmud Torah School`,
                    description: `The Talmud Torah School is a rabbinical school located in the new Mellah (Jewish quarter) of Meknes.`,
                    wiki:`https://en.wikipedia.org/wiki/Hebrew_School_Talmud_Torah_of_Meknes`,
                },
                ar: {
                    title:`مدرسة التلمود توره`,
                    description: `مدرسة تلمود توراة هي مدرسة تلمودية تقع في الملاح الجديد (الحي اليهودي) بمدينة  مكناس. `,
                    wiki:`https://ar.wikipedia.org/wiki/المدرسة_العبرانية_تلمود_توراة_مكناس`,
                },
            },
            {
                img: 'كنيس_ربي_ماير_طوليدانو_بمكناس-768x576.jpg',
                credit: 'ZAINEB HACHAMI   CC-BY-SA 4.0',
                fr: {
                    title:`Synagogue Rabbi Meir Tolédano`,
                    description: `Il s’agit de l’une des rares
                    synagogues de Meknès qui
                    sont toujours préservées.
                    La synagogue de Rabbi Meir
                    Tolédano fut édifié
                    initialement au 13ème siècle
                    et détruite par un
                    tremblement de terre en
                    1630.
                    Par la suite, elle fut
                    reconstruite par la famille des
                    Tolédano, d'où son nom.`,
                    wiki:`https://fr.wikipedia.org/wiki/Snagogue_Rabbi_Meir_Toledano_de_Meknes`,
                },
                en: {
                    title:`Synagogue of Rabbi Meir Toledano`,
                    description: `This synagogue is one of the few synagogues in Meknes that are still preserved. It  was originally built in the 13th century and destroyed by an earthquake in 1630. Then it was rebuilt by the Tolédano family, hence its name.`,
                    wiki:`https://en.wikipedia.org/wiki/Rabbi_Meir_Toledano_Synagogue_in_Meknes`,
                },
                ar: {
                    title:`كنيس ربي مير طوليدانو`,
                    description: `يعد كنيس مير طوليدانو واحدا من الكنس القليلة التي مازالت قائمة في مدينة مكناس.
                    و قد تم بناءه في الأصل خلال القرن الثالث عشر ودمر بفعل زلزال سنة 1630.
                    بعد ذلك ، قا<i>م</i>ت عائلة طوليدانو بإعادة بناءه ، ومن هنا جاء اسمه .                  
                    `,
                    wiki:` https://ar.wikipedia.org/wiki/كنيس_ربي_مير_طوليدانو_بمكناس`,
                },
            },
            {
                img: 'الملاح_الجديد_02-1630x860.jpg',
                credit: 'ZAINEB HACHAMI CC-BY-SA 4.0 ',
                fr: {
                    title:`Cheikh Mwijo`,
                    description: `Cheikh Mwijo, de son vrai
                    nom Moshe Attias, est né à
                    Meknès (Maroc) en 1937 et
                    mort à Kiryat Atta (Israël) le 2
                    mai 2020. Il était l’un des plus
                    grands musiciens du folklore
                    juif marocain.
                    Comme beaucoup d’artistes
                    j<i>u</i>ifs marocains, Moshe Attias
                    descend d’une lignée de
                    chanteurs et compositeurs.`,
                    wiki:`https://fr.wikipedia.org/wiki/Cheikh_Mwijo`,
                },
                en: {
                    title:`Cheikh Mwijo`,
                    description: `Cheikh Mwijo, whose real name is Moshe Attias, was born in Meknes (Morocco) in 1937 and died in Kiryat Atta (Israel) on May 2, 2020. He was one of the greatest musicians of Moroccan Jewish folklore. Like many Moroccan Jewish artists, Moshe Attias descends from a line of singers and songwriters.`,
                    wiki:`https://en.wikipedia.org/wiki/Cheikh_Mwijo`,
                },
                ar: {
                    title:`الشيخ مويجو`,
                    description: `   ولد الشيخ مويجو ، واسمه الحقيقي موشيه عطية ، في مكناس (المغرب) عام 1937 وتوفي في كريات عطا (إسرائيل) في 2 ماي 2020. كان من أعظم موسيقيي الفولكلور اليهودي المغربي.
                    كشأن العديد من الفنانين اليهود المغاربة ، ينحدر موشيه عطية من سلالة من المطربين وكتاب الأغاني.
                  `,
                    wiki:`https://ar.wikipedia.org/wiki/الشيخ_مويجو`,
                },
            },
        ]



    },

    RA: {
        latitude: 43,
        longitude: -93,
        cx: 533.3106007961919,
        cy: 191.66331271696865,
        img: `Late-Afternoon_View_over_Medina_Old_City_-_From_Hotel_Majestic_-_Rabat_-_Morocco_p.jpg`,

        pages: [
            {
                img: 'Late-Afternoon_View_over_Medina_Old_City_-_From_Hotel_Majestic_-_Rabat_-_Morocco-960x720.jpg',
                credit: 'Yamen, CC-BY-SA 4.0',
                fr: {
                    title:`Medina de Rabat`,
                    description: `Inscrites au patrimoine mondial de l'UNESCO depuis 2012, la Médina de Rabat, avec la Casbah des Oudayas et la ville moderne offrent un héritage partagé par plusieurs grandes cultures de l’histoire humaine : antique, islamique, hispano-maghrébine, européenne.`,
                    wiki:`https://fr.wikipedia.org/wiki/Médina_de_Rabat`,
                },
                en: {
                    title:`Medina of Rabat`,
                    description: `Listed as a UNESCO World Heritage Site since 2012, the Medina of Rabat, along with the Casbah of the Oudayas and the modern city, offer a heritage shared by several major cultures of human history: ancient, Islamic, Hispano-Maghreb, European.`,
                    wiki:`https://en.wikipedia.org/wiki/Medina_of_Rabat`,
                },
                ar: {
                    title:`مدينة الرباط`,
                    description: `تم إدراج المدينة العتيقة للرباط، مع قصبة الاوداية والمدينة الحديثة كموقع للتراث العالمي لليونسكو منذ عامً  2012 ،وهي تقدم تراثا مشتر ًك بين العديد من الثقافات : القديمة و الاسالمية  والاسبانية والمغاربية والاوروبية .`,
                    wiki:`https://ar.wikipedia.org/wiki/المدينة_العتيقة_للرباط`,
                },
            },
            {
                img: 'Rues_du_Mellah_de_Rabat_08112020_006-1280x1000.jpg',
                credit: 'mustapha_ennaimi CC-BY 2.0 ',
                fr: { 
                title:`Mellah de Rabat`,
                description: `Le Mellah est le quartier juif de Rabat, il est délimité au sud-ouest par la muraille andalouse et au nord-est par le rempart fluvial et la falaise.`,
                wiki:`https://fr.wikipedia.org/wiki/Mellah_de_Rabat`,
            },
                en: {
                    title:`Mellah of Rabat`,
                    description: `The Mellah is the Jewish quarter of Rabat, it is bounded to the southwest by the Andalusian wall and to the northeast by the river rampart and the cliff.`,
                    wiki:`https://en.wikipedia.org/wiki/Mellah_of_Rabat`,
                },
                ar: {
                    title:`ملاح الرباط`,
                    description: `الملاح هو الحي اليهودي بالرباط ، يحده من الجنوب الغربي الجدار الأندلسي ومن الشمال الشرقي سور النهر والجرف.`,
                    wiki:`https://ar.wikipedia.org/wiki/ملاح_الرباط`,
                },
            },
            {
                img: 'Synagogue_Rabbi_Chalom_Zaoui_Rabat_08112020_005-960x720.jpg',
                credit: 'Yamen, CC-BY-SA 4.0',
                fr: {
                    title:`Synagogue Rabbi Shalom Zaoui`,
                    description: `La synagogue de Rabbi Shalom Zaoui se situe à Bab Diwana, dans le Mellah de Rabat. Il S’agit initialement de la maison du Rabbin Shalom Zaoui, né au début du 19eme siècle et mort au début du 20ème. `,
                    wiki:`https://fr.wikipedia.org/wiki/Synagogue_Rabbi_Shalom_Zaoui`,
                },
                en: {
                    title:`Synagogue of Rabbi Shalom Zaoui`,
                    description: `Rabbi Shalom Zaoui's synagogue is located in Bab Diwana, in the Mellah of Rabat. It is initially the home of Rabbi Shalom Zaoui, born at the beginning of the 19th century and died at the beginning of the 20th. `,
                    wiki:`https://en.wikipedia.org/wiki/Rabbi_Shalom_Zaoui_synagogue`,
                },
                ar: {
                    title:`كنيس ربي شالوم الزاوي`,
                    description: `يقع كنيس ربي شالوم الزاوي في باب الديوانة، بملاح الرباط. و قد كان في الأساس منزل الحاخام شالوم الزاوي، الذي ولد في بداية القرن التاسع عشر و توفي في بداية القر<i>ن</i> العشرين.`,
                    wiki:`https://ar.wikipedia.org/wiki/كنيس_ربي_شالوم_الزاوي`,
                },
            },
            {
                img: 'Muraille_des_Andalous_1_-_Rabat-1200x675.jpg',
                credit: 'Omar-toons   CC-BY-SA 3.0 ',
                fr: {
                    title:`Muraille Andalouse de Rabat`,
                    description: `Cette muraille délimite la partie méridionale de la Médina. À sa construction au début du xviie siècle, elle délimite la partie où s'installent les réfugiés morisques de la partie sud de la cité almohade projetée.`,
                    wiki:`https://fr.wikipedia.org/wiki/Muraille_andalouse_de_Rabat`,
                },
                en: {
                    title:`Andalusian Wall of Rabat `,
                    description: `This wall delimits the
                    southern part of the Medina.
                    When it was built in the early
                    seventeenth century, it
                    delimits the part where the
                    Moors refugees from the
                    southern part of the planned
                    Almohad city se<i>t</i>tled.`,
                    wiki:`https://en.wikipedia.org/wiki/Andalusian_wall_of_Rabat`,
                },
                ar: {
                    title:`السور الأندلسي`,
                    description: `السور الأندلسي هو سور يحد الجزء الجنوبي من مدينة الرباط. بناه الموريسكيون الذين فروا من بطش المسيحيين مع بداية القرن السابع عشر.`,
                    wiki:`https://ar.wikipedia.org/wiki/السور_الأندلسي_بالرباط`,
                },
            },
            {
                img: 'Zaouia_in_the_old_Medina_of_Rabat_MedinaStories_2020_001-768x1074.jpg',
                credit: 'Yamen, CC-BY-SA 4.0',
                fr: {
                    title:`Les Zaouias de Rabat`,
                    description: `La Médina de Rabat abrite des dizaines de Zaouias s<i>o</i>ufies appartenant à différente confréries, comme les Aissawa, Tijanyine, Hmadcha, Touhamiyin, et Derqawa.`,
                    wiki:`https://fr.wikipedia.org/wiki/Les_Zaouias_de_Rabat`,
                },
                en: {
                    title:`The Zawiyas of Rabat`,
                    description: `La Médina de Rabat abrite des dizaines de Zaouias soufies appartenant à différente confréries, comme les Aissawa, Tijanyine, Hmadcha, Touhamiyin, et Derqawa.`,
                    wiki:`https://en.wikipedia.org/wiki/Zawiyas_of_Rabat`,
                },
                ar: {
                    title:`زوايا الصوفية بالرباط`,
                    description: `تحتضن مدينة الرباط عشرات الزوايا الصوفية المنتمية إلى طرائق مختلفة ، من بينها عيساوة و التيجانيون وحمادشة والتهاميون ودرقاوة.`,
                    wiki:`https://ar.wikipedia.org/wiki/الزوايا_الصوفية_بالرباط`,
                },
            },
            {
                img: 'Rue_des_consuls.jpg',
                credit: 'Ismael zniber  CC-BY-SA 3.0',
                fr: {
                    title:`Rue des Consuls`,
                    description: `La rue des Consuls est parmi les plus anciennes et les plus célèbres allées de la Médina de Rabat. Elle tient son nom du fait d'avoir été le lieu résidentiel des ambassadeurs et des consuls pendant la République de Salé puis sous l'Empire Chérifien jusqu'en 1912.`,
                    wiki:`https://fr.wikipedia.org/wiki/Rue_des_Consuls`,
                },
                en: {
                    title:`Consuls Alley`,
                    description: `Consuls Alley is one of the oldest and most famous alleys in the Medina of Rabat. It takes its name from having been the residential place of ambassadors and consuls during the Republic of Salé then under the Cherifian Empire until 1912.`,
                    wiki:`https://en.wikipedia.org/wiki/Consuls_Alley`,
                },
                ar: {
                    title:`شارع القناصل`,
                    description: `يعد شارع القناصل أحد أشهر الأزقة بالمدينة العتيقة للرباط. و يعود اسمه لكونه كان مقر سكنى السفراء و القناصل في عهد جمهورية سلا، و لاحقا في عهد الإيالة الشريفة إلى غاية 1912.`,
                    wiki:`https://ar.wikipedia.org/wiki/شارع_القناصل`,
                },
            },
        ]

    },



    SO: {
        latitude: 38.1,
        longitude: -48.5,
        cx: 1262.6296971623894,
        cy: 100.72068469313848,
        img: `Sousse_Kasbah_p.jpg`,
        pages: [
            {
                img: '800px-Medina_of_Sousse_Aerial_View.jpg',
                credit: 'Hamed Gamaoun - Own work, CC BY-SA 4.0',
                fr: {
                    title: `Medina de Sousse`,
                    description: `Inscrite au patrimoine mondial de l'UNESCO depuis 1988, La médina de Sousse constitue un exemple éminent de l’architecture arabomusulmane et méditerranéenne qui reflète un mode de vie traditionnel particulier.`,
                    wiki:`https://fr.wikipedia.org/wiki/Médina_de_Sousse`,
                },
                en: {
                    title: `Medina of Sousse`,
                    description: `Listed as a UNESCO World Heritage Site since 1988, the Medina of Sousse is an eminent example of Arab-Muslim and Mediterranean architecture which reflects a particular traditional way of life.`,
                    wiki:`https://en.wikipedia.org/wiki/Medina_of_Sousse,_Tunisia`,
                },
                ar: {
                    title:`مدينة سوسة`,
                    description: `تم إدراج مدينة سوسة كموقع تراث عالمي لليونسكو منذ عام 1988 ،وهي نموذج بارز للعمارة العربية الاسالمية والمتوسطية التي تعكس نمط عيش تقليدي متفرد .`,
                    wiki:`https://ar.wikipedia.org/wiki/مدينة_سوسة_العتيقة`,
                },
            },
            {
                img: 'Door_of_Grande_synagogue_in_Sousse-681x1024.jpg',
                credit: 'Hamed Gamaoun CC-BY-SA-4.0 ',
                fr: {
                    title:`Synagogue Kether Torah de Sousse`,
                    description: `La Synagogue Kether Torah
                    de Sousse a été construite
                    par Yossef Guez, le grand
                    rabbin de Sousse, en 1881.
                    Elle servira comme la
                    principale synagogue de la
                    communauté juive de la ville.
                    La ville de Sousse a connu
                    une importante communauté
                    juive, un recensement datant
                    de 1853 faisait état de 400
                    familles juives. Il y avait dans
                    la ville 6 synagogues,
                    actuellement seule Kether
                    Torah est resté en activité`,
                    wiki:`https://fr.wikipedia.org/wiki/Synagogue_Keter_Torah_de_Sousse`,
                },
                en: {
                    title:`Kether Torah Synagogue of Sousse`,
                    description: `<i>T</i>he Sousse Kether Torah Synagogue was built by Yossef Guez, the chief rabbi of Sousse, in 1881. It will serve as the main synagogue for the city's Jewish community.The city of Sousse used to have a large Jewish community, a census dating from 1853 reported 400 Jewish families. There were 6 synagogues in the city, but currently only Kether Torah remains in service.`,
                    wiki:`https://en.wikipedia.org/wiki/Kether_Torah_Synagogue_of_Sousse`,
                },
                ar: {
                    title:`كنيس كتر التوراة بسوسة `,
                    description: `تم بناء كنيس سوسة كيثر توراه من قبل يوسف غيز ، حاخام مدينة سوسة ، في عام 1881 ليكون الكنيس اليهودي الرئيسي ليهود المدينة.
                    كانت مدينة سوسة تضم جالية يهودية كبيرة ، فقد أفاد إحصاء يعود تاريخه إلى عام 1853 بوجود 400 عائلة يهودية.  كما كان هناك 6 كنائس يهودية في المدينة بقي متها حاليا إلا كيثر توراه .
                    `,
                    wiki:`https://ar.wikipedia.org/wiki/كنيس_كتر_التوراة_بسوسة`,
                },
            },
            {
                img: 'Zaouia_of_Sidi_Bouraoui_door.jpg',
                credit: 'Hamed Gamaoun - Own work, CC BY-SA 4.0',
                fr: {
                    title:`Zaouia de Sidi Bouraoui`,
                    description: `La zaouia de Sidi Bouraoui se
                    trouve au centre de la médina
                    de Sousse et elle porte le
                    nom du saint patron et
                    protecteur de la ville Sidi
                    Bouraoui un marabout
                    d'origine marocaine qui s'est
                    installé pendant l'époque
                    hafside (xve siècle).<br>
                    La zaouia abrite le tombeau
                    du saint que les locaux
                    continuent à le visiter et
                    solliciter sa grâce notamment
                    pendant les grands moments de la vie: mariages,
                    circoncisions, fêtes
                    religieuses`,
                    wiki:`https://fr.wikipedia.org/wiki/Mausolée_Sidi_Bouraoui`,
                },
                en: {
                    title:`Zawiya of Sidi Bouraoui`,
                    description: `The zaouia of Sidi Bouraoui is located in the center of the medina of Sousse and it's named after the saint patron and protector of the city: Sidi Bouraoui a marabout of Moroccan origin who settled during the Hafsid period (fifteenth century). The zaouia houses the tomb of the saint whos is still venerated by the locals who continue to visit his tomb and ask for his grace, especially during the importand moments of life: weddings, circumcisions, religious festivals`,
                    wiki:`https://en.wikipedia.org/wiki/Zawiya_of_Sidi_Bouraoui`,
                },
                ar: {
                    title:`زاوية سيدي بوراوي`,
                    description: `  تقع زاوية سيدي بوراوي في وسط المدينة العتيقة بسوسة وسميت على اسم حامي المدينة ورمزها: سيدي بوراوي ، الولي ذو الأصول المغربية و الذي استقر في سوسة خلال العهد الحفصي (القرن الخامس عشر).
                    تضم الزاوية قبر الولي الصالح الذي لا يزال يتمتع بشعبية لدى السكان المحليين الذين يواصلون زيارة قبره ويطلبون بركته، خاصة خلال لحظات الحياة المهمة: الأعراس والختان والاحتفالات الدينية
                    `,
                    wiki:`https://ar.wikipedia.org/wiki/زاوية_سيدي_بوراوي`,
                },
            },
            {
                img: 'Madrassa_Zeqqaqia-768x1200.jpg',
                credit: 'Hbensghaier CC-BY-SA 4.0',
                fr: {
                    title:`Medersa Zakkak`,
                    description: `Se trouvant pas loin de la
                    mosquée Eddamous, la
                    medersa <i>Z</i>akkak, qui abrite
                    également un mausolée et
                    une salle de prière, est un
                    édifice qui date de l'époque
                    aghlabide et qui a été
                    transformée pendant l'époque
                    Hafside en collège religieux.
                    Pendant le xviie siècle, les
                    ottomans lui ont ajouté un
                    minaret avec une forme
                    orthogonale et des façades
                    richement décorés par des
                    céramiques.`,
                    wiki:`https://fr.wikipedia.org/wiki/Medersa_Zakkak`,
                },
                en: {
                    title:`Medersa Zakkak`,
                    description: `Located not far from the Eddamous Mosque, the Zakkak Madrasa, which also houses a mausoleum and a prayer room, is a building which dates from the Aghlabid era and which was transformed during the Hafside era into a religious school. During the 17th century, the Ottomans added to it a minaret with an orthogonal shape.`,
                    wiki:`https://en.wikipedia.org/wiki/Medersa_Zakkak`,
                },
                ar: {
                    title:`مدرسة الزقاق`,
                    description: `تقع مدرسة الزقاق بالقرب من مسجد الداموس ، وهي تضم أيضًا ضريحًا وغرفة للصلاة ،
                    المبنى يعود تاريخه إلى العصر الأغلبي وتم تحويله في العهد الحفصي إلى مدرسة دينية.
                  خلال القرن السابع عشر ، أضاف العثمانيون إليها مئذنة على شكل متعامد.
                  .`,
                    wiki:`https://ar.wikipedia.org/wiki/مدرسة_الزقاق`,
                },
            },
            {
                img: 'Sousse_baptismal_font_02-768x1024.jpg',
                credit: 'Ad Meskens CC-BY-SA 3.0',
                fr: {
                    title:`Musée archéologique de Sousse`,
                    description: `Le musée archéologique de
                    Sousse est fondé en 1951 et
                    occupe une partie de
                    l'ancienne kasbah de Sousse.
                    Il comprend une importante
                    collection de mosaïques romaines et d'objets datant de
                    l'époque paléo-chrétienne
                    dont un baptistère découvert
                    découvert à quelques
                    kilomètre de Sousse et daté
                    du VIe siècle.`,
                    wiki:`https://fr.wikipedia.org/wiki/Musée_archéologique_de_Sousse`,
                },
                en: {
                    title:`Sousse Archaeological Museum `,
                    description: `The museum is housed in the Kasbah of Sousse's Medina and it was established in 1951. It contains an important collection of Roman mosaics and artifacts dating from the Paleo-Christian era, including a beautiful baptistery discovered a few kilometers away from Sousse and dated from the 6th century.`,
                    wiki:`https://en.wikipedia.org/wiki/Sousse_Archaeological_Museum`,
                },
                ar: {
                    title:`المتحف الأثري بسوسة`,
                    description: `تم تأسيس المتحف الأثري بسوسة في 1951، ويحتل جزءا من القصبة القديمة
                    يحتوي المتحف على مجموعة هامة من الفسيفساء الرومانية والتحف التي يعود تاريخها إلى العصر المسيحي القديم ، بما في ذلك معمودية جميلة تم اكتشافها على بعد بضعة كيلومترات من سوسة ويرجع تاريخها إلى القرن السادس ميلادي.
                    `,
                    wiki:`https://ar.wikipedia.org/wiki/المتحف_الأثري_بسوسة`,
                },
            }
          
        ]


    },


    TE: {
        latitude: 46.2,
        longitude: -89.00116,
        cx: 591.8509805948759,
        cy: 118.4304643360575,
        img: `Jewish_cemetery_of_Tetouan_p.jpg`,
        pages: [
            {
                img: 'Jewish_cemetery_of_Tetouan-1024x683.jpg',
                credit: 'TomiValny - Own work, CC BY-SA 4.0 ',
                fr: {
                    title:`Medina de Tétouan`,
                    description: `Inscrite au patrimoine mondial de l'UNESCO depuis 1997, la Medina de Tétouan fut principal point de jonction entre le Maroc et l'Andalousie, à point que la ville est encore surnommée de nos jours « la fille de Grenade » ou enco`,
                    wiki:`https://fr.wikipedia.org/wiki/Médina_de_Tétouan`,
                },
                en: {
                    title:`Medina of Tetouan`,
                    description: `Listed as a UNESCO World Heritage Site since 1997, the Medina of Tetouan was the main junction point between Morocco and Andalusia, so much so that the city is still nicknamed today 'the daughter of Granada' or 'the little one. Jerusalem '.`,
                    wiki:`https://en.wikipedia.org/wiki/Medina_of_Tetuan`,
                },
                ar: {
                    title:`مدينة تطوان`,
                    description: ` تم إدراج مدينة تطوان كموقع للتراث العالمي لليونسكو منذ عام 1997 وكانت نقطة اللتقاء الرئيسية بين المغرب والاندلس ، لدرجة أن المدينة لا تزال تلقب اليوم ابنة غرناطة أو القدس الصغيرة .`,
                    wiki:`https://ar.wikipedia.org/wiki/المدينة_العتيقة_لتطوان`,
                },
            },
            {
                img: 'معبد_إسحاق_ابن_وليد_2-768x1024.jpg',
                credit: 'Sambasoccer27 CC-BY-SA 4.0',
                fr: {
                    title:`Synagogue Isaac Ben Walid`,
                    description: `La Synagogue Isaac Ben
                    Walid est une synagogue
                    dans le mellah de Tétouan.
                    La synagogue porte le nom
                    du rabbin Isaac Ben Walid
                    (1777–1870) un membre
                    éminent de la communauté
                    juive de Tétouan, qui comptait
                    environ 4200 membres à
                    l'époque.`,
                    wiki:`https://fr.wikipedia.org/wiki/Synagogue_Isaac_Ben_Walid`,
                },
                en: {
                    title:`Isaac Ben Walid Synagogue`,
                    description: `The Isaac Ben Walid Synagogue is a synagogue in the mellah of Tetuan. The synagogue is named after Rabbi Isaac Bengualid (1777–1870), author of the 2-volume Vayomer Yitzhak, a history of the Jews of Tetuan. Rabbi Bengualid was a prominent member of Tetuan's Jewish community, which comprised about 4200 members at the time.`,
                    wiki:`https://en.wikipedia.org/wiki/Isaac_Ben_Walid_Synagogue`,
                },
                ar: {
                    title:`كنيس إسحاق ابن وليد`,
                    description: `يقع كنيس إسحاق ابن وليد  في ملاح مدينة تطوان..
                    سمي الكنيس تيمنا بالحاخام إسحاق ابن الوليد وهو صاحب كتاب ذي مجلدين عن تاريخ يهود تطوان، واسم الكتاب "وقال إسحاق." كان من أبرز أعضاء المتجتمع اليهودي والذي بلغ 4200 نسمة آنذاك
                   `,
                    wiki:`https://ar.wikipedia.org/wiki/كنيس_إسحاق_ابن_الوليد`,
                },
            },
            {
                img: 'Zaouia_Harrakia_MedinaStories_10112020-1028x1536.jpg',
                credit: 'Nuruddin CC-BY 2.5',
                fr: {
                    title:`Zaouïa Harrakia`,
                    description: `La Zaouïa Harrakia est une
                    zaouïa située à la médina de
                    Tétouan.
                    Élevée au milieu du xixe
                    siècle, la zaouïa est
                    consacrée à la tariqa Harrakia
                    ; elle porte le nom du cheikh
                    de la tariqa (chef de la tariqa)
                    Sidi Mohamed Harrak.`,
                    wiki:`https://fr.wikipedia.org/wiki/Zaouïa_Harrakia`,
                },
                en: {
                    title:`Zawiya Harrakia of Tetuan`,
                    description: `The Zaouïa Harrakia is a zaouïa located in the medina of Tetouan. Built in the mid-nineteenth century, the <i>Z</i>aouïa is devoted to the tariqa Harrakia; it's named after the sheikh of the tariqa (head of the tariqa) Sidi Mohamed Harrak.`,
                    wiki:`https://en.wikipedia.org/wiki/Zawiya_Harrakia_of_Tetuan`,
                },
                ar: {
                    title:`زاوية الحراق بتطوان`,
                    description: `الزاوية الحراقية هي زاوية تقع في مدينة تطوان.
                    بنيت  الزاوية في منتصف القرن التاسع عشر وهي تنتمي للطريقة الحراقية. وقد سميت على اسم شيخ الطريقة سيدي محمد الحراق.
                      `,
                    wiki:`https://ar.wikipedia.org/wiki/زاوية_الحراق_بتطوان`,
                },
            },
            {
                img: 'Casa_Aragon-683x1024.jpg',
                credit: 'Imane aragon CC-BY-SA 4.0',
                fr: {
                    title:`Casa Aragon `,
                    description: `La Casa Aragon est un
                    exemple des maisons
                    Andalouses appartenant à
                    une famille andalouse.
                    Constituée d’un patio avec 12
                    colonnes datant du XVIIIème
                    siècle, elle ab<i>r</i>ite aujourd’hui
                    l'Organisation nationale des
                    aveugles et est constituée
                    d'une résidence-école pour
                    enfants aveugles de la ville. `,
                    wiki:`https://fr.wikipedia.org/wiki/Casa_Aragon_(Tétouan)`,
                },
                en: {
                    title:`Casa Aragon`,
                    description: `The houses in the Medina are generally very well preserved by the descendants of the owners who some of them are from Andalusian origin. 
                    Some of these houses have benefited from a rehabilitation program funded by the Regional Government of Andalusia.
                    The Casa Aragon is an example of these houses belonging to an Andalusian family. Made up of a patio with 12 columns dating from the 18th century, it houses now  the National Organization for the Blind and is a residence-school for blind children in the city.
                      `,
                    wiki:`https://en.wikipedia.org/wiki/Casa_Aragon_(Tetouan)`,
                },
                ar: {
                    title:`دار أرغون`,
                    description: `تتميز منازل مدينة تطوان القديمة بحالتها الجيدة حيث تتمتع بالعناية من قبل أحفاد أصحابها ذو الأصل الأندلسي.
                    وقد استفادت بعض هذه المنازل من برنامج إعادة التأهيل الممول من قبل حكومة الأندلس الإقليمية.                    
                    تعتبر دار أراغون مثالاً على هذه المنازل التي كانت تمتلكها عائلة أندلسية و هي عائلة أراغون. يتكون المنزل من فناء و 12 عمودًا تعود إلى القرن الثامن عشر ، وهو الآن مقر المنظمة الوطنية للمكفوفين ومدرسة إقامة للأطفال المكفوفين.
                    `,
                    wiki:`https://ar.wikipedia.org/wiki/دار_أراغون_(تطوان)`,
                },
            },
            {
                img: 'Medina_de_Tetouan_Nov2020_016-1024x930.jpg',
                credit: 'Yamen, CC-BY-SA 4.0 ',
                fr: {
                    title:`Medersa Loukach`,
                    description: `La Medersa Loukach est une
                    médersa située en plein cœur
                    de l’ancienne Medina de
                    Tétouan.
                    Le bâtiment date du XVIIIe
                    siècle et il a été construit par
                    le gouverneur de Tétouan de
                    1750 à 1757 le caïd
                    Mohamed Loukach.
                    Aujourd'hui la medersa abrite
                    le musée du patrimoine
                    religieux dont l’objectif est de
                    mettre en évidence les
                    principes de tolérance et
                    d’ouverture de l’islam. `,
                    wiki:`https://fr.wikipedia.org/wiki/Medersa_Loukach`,
                },
                en: {
                    title:`Medersa Loukach `,
                    description: `The Medersa of Loukach is a medersa located in the heart of the old Medina of Tetouan. The building dates from the 18th century and it was built by the governor of Tetouan the caïd Mohamed Loukach. from 1750 to 1757 Today the medersa houses the museum of religious heritage whose objective is to highlight the principles of tolerance and openness of Islam.`,
                    wiki:`https://en.wikipedia.org/wiki/Medersa_Loukach`,
                },
                ar: {
                    title:`مدرسة لوقاش`,
                    description: `مدرسة لوقاش هي مدرسة تقع في قلب مدينة تطوان القديمة.
                    يعود تاريخ المبنى إلى القرن الثامن عشر ، وقد بناه والي تطوان القائد محمد لوكاش. من 1750 إلى 1757
                    تضم المدرسة اليوم متحف التراث الديني الذي يهدف إلى إبراز مبادئ التسامح والانفت<i>ا</i>ح في الإسلام.`,
                    wiki:`https://ar.wikipedia.org/wiki/مدرسة_لوقاش`,
                },
            
            },
        ]


    },


    TU: {
        latitude: 40.3,
        longitude: -48.5,
        cx: 1239.754885735428,
        cy: 58.41179455781675,
        img: `La_Médina_de_Tunis_p.jpg`,
        pages: [
            {
                img: 'La_Médina_de_Tunis-1200x675.jpg',
                credit: 'Mohamed Ali Sghaier - Own work, CC BY-SA 4.0',
                fr: {
                    title:`Medina de Tunis`,
                    description: `Inscrite au patrimoine mondial de l'UNESCO depuis 1979, la Medina de Tunis fut considérée comme l'une des villes les plus importantes et les plus riches du monde islamique.`,
                    wiki:`https://fr.wikipedia.org/wiki/Médina_de_Tunis`,
                },
                en: {
                    title:`Medina of Tunis`,
                    description: `Listed as a UNESCO World Heritage Site since 1979, the Medina of Tunis was considered one of the most important and richest cities in the Islamic world.`,
                    wiki:`https://en.wikipedia.org/wiki/Medina_of_Tunis`,
                },
                ar: {
                    title:`مدينة تونس `,
                    description: `تم إدراج مدينة تونس في قائمة مواقع التراث العالمي لليونسكو منذ عام 1979 ، وقد كانت واحدة من أهم وأغنى المدن  في العالم الاسالمي . `,
                    wiki:`https://ar.wikipedia.org/wiki/مدينة_تونس_العتيقة`,
                },
            },
            {
                img: 'BIBLIOTHÈQUE_DIOCÉSAINE_TN_03-960x720.jpg',
                credit: 'Youssefbensaad CC-BY-SA 4.0',
                fr: {
                    title:`Bibliothèque diocésaine de Tunis`,
                    description: `La Bibliothèque diocésaine de Tunis ou Bibliothèque des sciences religieuses de Tunis est une bibliothèque tunisienne située à Tunis. Dépendant de l'archidiocèse de Tunis et spécialisée dans les sciences des religions, elle a pour but de se mettre au service du dialogue entre les religions et les cultures. Elle se situe sur la rue Sidi Saber, au centre de la médina de Tunis, et occupe le rez-de-chaussée de l'ancienne école privée catholique Saint-Joseph des Sœurs de Saint-Joseph de l'Apparition.`,
                    wiki:`https://fr.wikipedia.org/wiki/Bibliothèque_diocésaine_de_Tunis`,
                },
                en: {
                    title:`Diocesan Library of Tunis`,
                    description: `The Diocesan Library of Tunis is a library located in Tunis. It is operated by the Archdiocese of Tunis and specializes in religious science, focused on facilitating dialogue between religions and cultures.
                    The libr<i>a</i>ry is located on Sidi Saber Street, where it occupies the basement of former Catholic school once operated by the Sisters of St. Joseph of the Apparition.
                    `,
                    wiki:`https://en.wikipedia.org/wiki/Diocesan_Library_of_Tunis`,
                },
                ar: {
                    title:`المكتبة الأسقفية بتونس`,
                    description: `المكتبة الأسقفية بتونس أو مكتبة العلوم الدينية بتونس ‏ هي مكتبة تونسية تقع في تونس العاصمة. ترجع ملكيتها لأبرشية تونس وهي مختصة في العلوم الدينية. هدفها خدمة حوار الأديان السماوية ومختلف الثقافات.
                    تقع المكتبة في نهج سيدي صابر في قلب مدينة تونس العتيقة، وتوجد في الطابق الأرضي لمدرسة القديس جوزيف الكاثوليكية التي كانت تابعة لمؤسسة أخوات القديس جوزيف الظهور
                    .`,
                    wiki:`https://ar.wikipedia.org/wiki/المكتبة_الأسقفية_بتونس`,
                },
            },
            {
                img: 'Grande_Synagogue_de_la_Hara_de_Tunis-1536x969.jpg',
                credit: 'Domaine public',
                fr: {
                    title:`Hara de Tunis `,
                    description: `La Hara actuellement
                    appelée Hafsia, a été le
                    quartier juif de la médina de
                    Tunis.
                    Hara, un mot du dialecte
                    arabe tunisien signifiant «
                    quatre », est en rapport avec
                    le nombre de familles juives
                    qui ont fondé le quartier, et
                    qui était quatre selon les
                    histoires populaires`,
                    wiki:`https://fr.wikipedia.org/wiki/Hara_(Tunis)`,
                },
                en: {
                    title:`Hara (Tunis)`,
                    description: `Hara, a word in the Tunisian Arabic dialect meaning "quarter", relates to the number of Jewish families who founded the neighborhood, which was four according to popular stories.`,
                    wiki:`https://en.wikipedia.org/wiki/Hara_(Tunis)`,
                },
                ar: {
                    title:`الحارة (تونس)`,
                    description: `الحارة وتُعرف حاليًا باسمِ حفصية، كانت الحي اليهودي في مدينةِ تونس العتيقة.
                    حارة هي كلمة من الدارجة التونسيّة وتعني «الحيّ»، والمقصود هنا بالحارة في تونس الحيّ الذي كانت تقطنهُ العائلات اليهودية والتي لربما هي من أسَّسَتْهُ بحسبِ الروايات الشعبيّة.
                       `,
                    wiki:`https://ar.wikipedia.org/wiki/الحارة_(تونس)`,
                },
            },
            {
                img: 'Église_anglicane_Saint-Georges-768x1024.jpg',
                credit: 'jamel eddine ben saidane alias Wildtunis CC-BY-SA 4.0',
                fr: {
                    title:`Église anglicane Saint-Georges`,
                    description: `L’église anglicane
                    Saint-Georges est située
                    dans le quartier populaire de
                    la Hara. Sa construction est
                    entamée à partir des années
                    1894-1896. Le modèle
                    archi<i>t</i>ectural est celui d'une
                    église anglicane située à
                    Patras. Elle est consacrée en
                    février 1901 par l'évêque de
                    Gibraltar.
                    Le colonel et consul général
                    britannique Thomas Reade,
                    en poste de 1824 à sa mort
                    en 1849 et dont l'épitaphe
                    rappelle qu'il contribua à faire
                    abolir l'esclavage en Tunisie
                    en 1845, y est enterré.`,
                    wiki:`https://fr.wikipedia.org/wiki/Église_anglicane_Saint-Georges_(Tunis)`,
                },
                en: {
                    title:`St George's Anglican Church`,
                    description: `St. George's Anglican Church is located in the popular Hara district. Built between 1894-1896 and it's very similar to an Anglican church located in Patras (Greece). It was consecrated in February 1901 by the Bishop of Gibraltar.
                    The British colonel and general consul Thomas Reade, in office from 1824 until his death in 1849 and whose epitaph recalls that he contributed to the abolition of slavery in Tunisia in 1845, is buried there.`,
                    wiki:`https://en.wikipedia.org/wiki/St._George%27s_Anglican_Church_(Tunis)`,
                },
                ar: {
                    title:`كنيسة القديس جورج الأنجليكانية بتونس`,
                    description: `كنيسة القديس جورج الأنجليكانية بتونس ‏ هي كنيسة أنجليكانية تقع في وسط  مدينة تونس
                    في سنة 1848م، أمر المشير أحمد باي الأول بإعادة بناء هذا المعبد وتوسيعه ليكون على هيئته الحالية، وقد تم نقل القبور إلى أماكن أخرى، وما زالت الطقوس الدينية تقام بهذه الكنيسة إلى يومنا هذا وتسهر على إدارته السفارة البريطانية بتونس
                    .`,
                    wiki:`https://ar.wikipedia.org/wiki/كنيسة_القديس_جورج_الأنجليكانية_بتونس`,
                },
            },
            {
                img: '800px-Mosquée_Hanfite_de_Mhamed_Bey-768x1365.jpg',
                credit: 'jamel eddine ben saidane alias Wildtunis CC-BY-SA 4.0',
                fr: {
                    title:`Mosquée Sidi Mahrez `,
                    description: `La mosquée Sidi Mahrez ou
                    mosquée M'hamed Bey est
                    un édifice religieux souvent
                    considéré comme la plus
                    belle mosquée de Tunis.
                    Elle tire son nom de La
                    zaouïa de Sidi Mahrez, saint
                    patron de la médina de
                    Tunis, qui se trouve en face.
                    Cette mosquée est
                    également de la plus
                    importante mosquée
                    Hanafite de la ville.`,
                    wiki:`https://fr.wikipedia.org/wiki/Mosquée_Sidi_Mahrez`,
                },
                en: {
                    title:`Sidi Mahrez Mosque`,
                    description: `The Sidi Mahrez mosque or M'hamed Bey mosque is a religious building often considered as the most beautiful mosque in Tunis. It takes its name from the Mausoleum of Sidi Mahrez, patron saint of the medina of Tunis, which is in front of it. This mosque is also the most important Hanafi mosque in the city.`,
                    wiki:`https://en.wikipedia.org/wiki/Sidi_Mahrez_Mosque`,
                },
                ar: {
                    title:`جامع سيدي محرز`,
                    description: ` جامع سيدي محرز هو جامع من جوامع مدينة ت<i>و</i>نس يقع في نهج سيدي محرز، و هو أكبر جامع للمذهب الحنفي.تم تشييد هذا الجامع على يد محمد باي المرادي، نجل مراد الثاني، لكنه يعرف باسم جامع سيدي محرز وذلك لوجوده أمام الزاوية التي تحمل اسم الرجل الصالح.`,
                    wiki:`https://ar.wikipedia.org/wiki/جامع_سيدي_محرز`,
                },
            },
            {
                img: 'Photo_Médina_de_Tunis_31102020_01-1280x1000.jpg',
                credit: 'Skander Mariem Sayah CC-BY-SA 4.0',
                fr: {
                    title:`Synagogue Or-Thora de Tunis `,
                    description: `La synagogue Or-Thora est
                    une ancienne synagogue
                    tunisienne située dans
                    l'ancien quartier de la Hara
                    (actuel quartier de la Hafsia),
                    sur la rue Achour, à Tunis.
                    <br>
                    Elle est construite par les
                    architectes Aimé Krief et Jean
                    Sebag avant la Seconde
                    Guerre mondiale.`,
                    wiki:`https://fr.wikipedia.org/wiki/Synagogue_Or-Thora_de_Tunis`,
                },
                en: {
                    title:`Or Thora Synagogue (Tunis)`,
                    description: `The Or Thora Synagogue is a former synagogue in Tunis, Tunisia. It was completed in the early 1930s, prior to World War II. It was designed by architects Aimé Krief and Jean Sebag.`,
                    wiki:`https://en.wikipedia.org/wiki/Or_Thora_Synagogue_(Tunis)`,
                },
                ar: {
                    title:`كنيس أور تورا بتونس`,
                    description: `كنيس أور تورا بتونس ‏، هو كنيس يهودي تونسي قديم وغير مستعمل، يقع في نهج عاشور في الحي اليهودي القديم في تونس العاصمة، الذي كان يسمى الحارة (حي الحفصية الآن).
                    <br>
                    تم تشييد الكنيس من قبل المهندسين المعماريين أيمي كرياف و جان فالونسي وذلك قبل الحرب العالمية الثانية.`,
                    wiki:`https://ar.wikipedia.org/wiki/كنيس_أور_تورا_بتونس`,
                },
            },
        ]

    },

    ZZ: {
        cx: -99999,
        cy: -99999,
        matrouz: true,
        pages: [
            {
                img: 'La_Médina_de_Tunis-1200x675.jpg',
                fr: {
                    title:`Le Matrouz`,
                    description: `Le Matrouz (mot d'origine arabe qui veut dire brodé), appelé aussi ou poésie brodée est un mode d’expression poétique culturel judéo-arabe qui s’est développé en Afrique du Nord à partir du XVI suite à l'expulsion des juifs et des musulmans de l’Andalousie. Le Matrouz représente un symbole du dialogue des cultures à travers cette juxtaposition du texte hébraïque au texte arabe et ces poèmes écrits par des auteurs juifs écrivant en langue arabe.`,
                    wiki:`https://fr.wikipedia.org/wiki/Matrouz_(musique)`,
                    caption: 'Nous espérons que vous avez fait un agréable voyage !',
                },
                en: {
                    title:`The Matrouz`,
                    description: `The Matrouz (arabic word which means embroidered), called also embroidered poetry is a mode of Judeo-Arab poetic and cultural expression developed in North Africa during the XVI following the expulsion of the Jews and Muslims from Andalusia. The Matrouz represents a symbol of dialogue among cultures through this juxtaposition of the Hebrew text to the Arabic and these poems written by Jewish authors and written in the Arabic language. `,
                    wiki:`https://en.wikipedia.org/wiki/Matrouz_(music)`,
                    caption: 'We hope that you had a wonderful journey !',
                },
                ar: {
                    title:`المطروز `,
                    description: `فن المطروز ، ويسمى أيضا الشعر المطرز ، هو أحد أشكال التعبير الشعري والثقافي اليهودي العربي الذي ظهر في شمال إفريقيا خلال القرن السادس عشر .بعد طرد اليهود والمسلمين من الاندلس يمثل المطروز رمزا من رموز الحوار بين الثقافات من خلال هذه المراوحة بين النص العبري والعربية والقصائد التي .كتبها مؤلفون يهود باللغة العربية`,
                    wiki:`https://ar.wikipedia.org/wiki/فن_المطروز`,
                    caption: 'نتمنى أن تكونوا قد قضيتم رحلة ممتعة!',
                },
            },
        ]
    },
}

